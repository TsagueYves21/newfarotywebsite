import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import MyCollectView from "../views/MyCollectVeiw.vue";
import ConnectionView from "../views/ConnectionView.vue";
import CrowfundingView from "../views/CrowfundingView.vue";
import WebPagesView from "../views/WebPagesView.vue";


const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/mycollect",
    name: "mycollect",
    component:MyCollectView,
  },
  {
    path: "/connection",
    name: "connection",
    component: ConnectionView,
  },
  {
    path: "/crowfunding",
    name: "crowfunding",
    component: CrowfundingView,
  },
  {
    path: "/webpages",
    name: "webpages",
    component: WebPagesView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ConnectionView.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
